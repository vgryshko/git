<?php

use Service\Auth;

if ($user = Auth::login($_POST)) {
    $_SESSION['user'] = $user;
    redirect('/dashboard');
} else {
    redirect('/login.php');
}