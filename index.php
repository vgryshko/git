<?php

use Model\User;

$userId = (int)$_GET['user_id'];

$user = User::findFirst($userId);

if (!$user) {
    throw new NotFoundException('User not found');
}

echo $view->render('user.volt', [
    'user' => $user,
]);