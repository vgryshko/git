<?php

use Model\User;

$form = new Form\User\Signup();

if (!$form->validate($_POST)) {
    throw new ValidateException('Signup form validation failed');
}

$user = new User();
$user->save($form->getValues());

redirect('/dashboard');